Visualizza le viste logiche di un file fisico (DBR)                    ---

DBR001C - Programma principale. Chiede nome file fisico e libreria.
Esegue il controllo che il file immesso sia un file fisico.
Richiama DBR001R.

DBR001R - Visualizza i campi che compongono il tracciato del file.
Per prima cosa verifica se il fisico ha pi di un tracciato, ed
in questo caso chiede di scegliere quale si vuole utilizzare.
Scelto il tracciato vengono visualizzati tutti i campi,
ho la possibilita di vedere le viste logiche collegate (DBR002R),
di stampare i campi del tracciato (DBR8R),
di visualizzare gli attributi del file fisico (DBR5R).

DBR002R -Visualizza viste collegate al fisico

DBR003R -Filtra le viste in base alla scelta campi.

DBR5R -Visualizza gli attributi del file fisico. Se il fisico ha
chiavi o trigger collegati hai la possibilit di visualizzarli
premendo il rispettivo tasto di comando.


DBR6R -Visualizza l'elenco delle chiavi del file (fisico o logico).

DBR7R -Visualizza l'elenco dei trigger collegati al file fisico.


DBR8R -Stampa i campi del tracciato del file fisico
